<?php

/**
 * @file
 * Admin pages
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form.
 */
function theme_context_settings() {
  $form = array();

  $form['fieldset'] = array(
    '#title' => t('Selection'),
    '#type' => 'fieldset',
  );

  $form['fieldset']['theme_context_enable'] = array(
    '#title' => t('Enable'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('theme_context_enable', FALSE),
  );

//  $cache = variable_get('cache', 0);
//
//  $markup = '';
//  if ($cache) {
//    $markup .= t('<p>Page cache: <code>Enabled</code></p>');
//  }
//  else {
//    $markup .= t('<p>Page cache: <code>Disabled</code></p>');
//  }
//  $markup .= t('<p>It is not possible for anonymous users to have different themes for the same page if caching is enabled, e.g. web crawlers and normal users.</p>');
//
//  $form['notice'] = array(
//    '#markup' => $markup,
//  );

  return system_settings_form($form);
}

/**
 * Settings form
 */
function theme_context_theme_settings($form_id, $form_state, $theme) {
  $form = array();

  $themes = list_themes();

  $theme_default_name = variable_get('theme_default', NULL);
  $theme_default = array_key_exists($theme_default_name, $themes) ? $themes[$theme_default_name] : NULL;

  $theme_admin_name = variable_get('admin_theme', NULL);
  $theme_admin = array_key_exists($theme_admin_name, $themes) ? $themes[$theme_admin_name] : NULL;

  $default_whitelist = ($theme === $theme_admin_name ? theme_context_get_admin_paths() : '');
  $default_blacklist = ($theme === $theme_admin_name ? '' : theme_context_get_admin_paths());

  $form['path'] = array(
    '#title' => t('Selection'),
    '#type' => 'fieldset',
    '#description' => t('<p>Default theme: <code>@theme_default</code>. Administration theme: <code>@theme_admin</code>.</p>', array(
      '@theme_default' => empty($theme_default) ? t('Not set') : $theme_default->info['name'],
      '@theme_admin' => empty($theme_admin) ? t('Not set') : $theme_admin->info['name'],
    )),
  );

  $form['path']['theme_context_enable_' . $theme] = array(
    '#title' => t('Available for selection'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('theme_context_enable_' . $theme, FALSE),
  );

  $form['path']['theme_context_whitelist_' . $theme] = array(
    '#type' => 'textarea',
    '#title' => t('Whitelist'),
    '#description' => t('A list of Drupal paths where this theme should be used, one on each line. Leave empty to apply the configuration set on the <a href="@url">Appearance page</a>.', array(
      '@url' => url('admin/appearance'),
    )),
    '#default_value' => variable_get('theme_context_whitelist_' . $theme, $default_whitelist),
    '#rows' => 10,
  );

  $form['path']['theme_context_blacklist_' . $theme] = array(
    '#type' => 'textarea',
    '#title' => t('Blacklist'),
    '#description' => t('A list of Drupal paths where this theme should not be used, one on each line. Use this field to negate included paths, e.g. <code>@key</code> array returned by <code>@function</code>.', array(
      '@url' => url('admin/appearance'),
      '@key' => 'non_admin',
      '@function' => 'path_get_admin_paths()',
    )),
    '#default_value' => variable_get('theme_context_blacklist_' . $theme, $default_blacklist),
    '#rows' => 10,
  );

  $paths = path_get_admin_paths();
  $form['path']['path_help'] = array(
    '#title' => t('@function', array('@function' => 'path_get_admin_paths()')),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $list = explode("\n", $paths['admin']);
  sort($list);
  $markup = "admin => \n  " . str_replace("\n", "\n  ", implode("\n", $list));
  $markup .= "\n\n";
  $list = explode("\n", $paths['non_admin']);
  sort($list);
  $markup .= "non_admin => \n  " . str_replace("\n", "\n  ", implode("\n", $list));
  $form['path']['path_help']['help'] = array(
    '#markup' => '<pre>' . $markup . '</pre>',
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the theme settings form
 */
function theme_context_theme_settings_validate($form, &$form_state) {
  $theme = $form_state['build_info']['args'][0];

  // Normalize newline character and remove empty lines
  $keys = array(
    'theme_context_whitelist_' . $theme,
    'theme_context_blacklist_' . $theme,
  );
  foreach ($keys as $key) {
    $value = $form_state['values'][$key];
    $value = str_replace(array("\r\n", "\r"), "\n", trim($value));
    $value = preg_replace('/^\s*/m', '', $value);
    $arr = explode("\n", $value);
    sort($arr);
    $value = implode("\n", $arr);
    $form_state['values'][$key] = $value;
  }
}
