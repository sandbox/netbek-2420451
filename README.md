# theme_context

Set theme by URL.

It's not possible for anonymous users to have different themes for the same page if page caching is enabled, e.g. web crawlers and normal users.
