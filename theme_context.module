<?php

/**
 * @file
 * Theme Context
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Implements hook_menu().
 */
function theme_context_menu() {
  $items = array();

  $items['admin/appearance/theme-context'] = array(
    'title' => 'Context',
    'description' => 'Configuration for Theme Context.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('theme_context_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'theme_context.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/appearance/theme-context/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('theme_context_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'theme_context.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );

  $theme_default = variable_get('theme_default', '');
  $enabled_themes = array();
  $themes = list_themes();

  foreach ($themes as $theme) {
    if ($theme->status && strcasecmp($theme->name, $theme_default) === 0) {
      $enabled_themes[] = $theme;
    }
  }
  foreach ($themes as $theme) {
    if ($theme->status && strcasecmp($theme->name, $theme_default) !== 0) {
      $enabled_themes[] = $theme;
    }
  }

  foreach ($enabled_themes as $theme) {
    $items['admin/appearance/theme-context/' . $theme->name] = array(
      'title' => $theme->info['name'],
      'description' => 'Configure context for ' . $theme->info['name'],
      'page callback' => 'drupal_get_form',
      'page arguments' => array('theme_context_theme_settings', $theme->name),
      'access arguments' => array('administer site configuration'),
      'file' => 'theme_context.admin.inc',
      'type' => MENU_NORMAL_ITEM,
    );
    $items['admin/appearance/theme-context/' . $theme->name] = array(
      'title' => $theme->info['name'],
      'description' => 'Configure context for ' . $theme->info['name'],
      'page callback' => 'drupal_get_form',
      'page arguments' => array('theme_context_theme_settings', $theme->name),
      'access arguments' => array('administer site configuration'),
      'file' => 'theme_context.admin.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 2,
    );
  }

  return $items;
}

/**
 * Implements hook_custom_theme().
 */
function theme_context_custom_theme() {
  if (variable_get('theme_context_enable', FALSE)) {
    return theme_context_get_theme(current_path());
  }
}

/**
 * Returns a list of paths that should always be whitelisted for admin themes.
 *
 * @return string
 */
function theme_context_get_admin_paths() {
  $whitelist = &drupal_static(__FUNCTION__);

  if (!isset($whitelist)) {
    $admin_paths = path_get_admin_paths();

    $whitelist = $admin_paths['admin'] . "\n" . $admin_paths['non_admin'];
    $whitelist = preg_replace('/^\s*/m', '', $whitelist);
    $whitelist = explode("\n", $whitelist);
    $whitelist = array_unique($whitelist);
    sort($whitelist);
    $whitelist = implode("\n", $whitelist);
  }

  return $whitelist;
}

/**
 * Get the theme name for the path.
 *
 * @param string $path
 * @return string|null
 */
function theme_context_get_theme($path) {
  $themes = list_themes();

  foreach ($themes as $theme) {
    if ($theme->status && TRUE === (bool) variable_get('theme_context_enable_' . $theme->name, FALSE)) {
      if (_theme_context_get_theme($theme->name, $path)) {
        return $theme->name;
      }
    }
  }

  if (path_is_admin($path)) {
    $theme = variable_get('admin_theme', NULL);
    if ($theme !== NULL) {
      return $theme;
    }
  }

  return variable_get('theme_default', NULL);
}

/**
 *
 * @param string $theme_name
 * @param string $path
 * @return boolean
 */
function _theme_context_get_theme($theme_name, $path) {
  $whitelist = variable_get('theme_context_whitelist_' . $theme_name, '');

  // Only check whitelist if it is not empty.
  if (!empty($whitelist)) {
    if (!theme_context_check_path_visibility(1, $whitelist, $path)) {
      return FALSE;
    }
  }

  $blacklist = variable_get('theme_context_blacklist_' . $theme_name, '');

  // Only check blacklist if it is not empty.
  if (!empty($blacklist)) {
    if (theme_context_check_path_visibility(1, $blacklist, $path)) {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 *
 * @param int $visibility
 * @param string $pages
 * @param string $path Path to check. If empty, then current path is checked.
 * @return boolean
 */
function theme_context_check_path_visibility($visibility, $pages, $path = NULL) {
  if (empty($pages)) {
    return FALSE;
  }

  if ($path === NULL) {
    $path = $_GET['q'];
  }

  $pages = drupal_strtolower($pages);
  $alias = drupal_strtolower(drupal_get_path_alias($path));
  $match = drupal_match_path($alias, $pages);

  if ($alias != $path) {
    $match = $match || drupal_match_path($path, $pages);
  }

  return !($visibility xor $match);
}
